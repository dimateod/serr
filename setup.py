from setuptools import find_packages, setup


REQUIRES = [
    "click",
    "PyYAML",
    "urwid",
]

setup(name='searchlist',
      version='1.0',
      author='Teodor Dima',
      author_email='dima.teod@gmail.com',
      license='Apache 2.0',
      packages=find_packages(),
      install_requires=REQUIRES,
      classifiers=[
          "Programming Language :: Python :: 3.4",
          "Environment :: Console",
          "License :: OSI Approved :: Apache Software License",
          "Development Status :: 4 - Beta",
      ],
      entry_points={
          "console_scripts": [
              "serr = searchlist.cli:main"
          ]
      },
      zip_safe=True)

