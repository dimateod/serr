import sys
import os

import yaml

from searchlist import LOG
from searchlist.config import settings


class Data(list):
    """Collection of `FileItem`s. Autosaves if one `FileItem` is modified and
    `save_path` is set.

    """
    def __init__(self, *a, **k):
        super().__init__(*a, **k)
        self.save_path = None

    def append(self, item):
        super().append(item)
        item.parent = self

    def save(self):
        if self.save_path:
            with open(self.save_path, "w") as dumpf:
                yaml.safe_dump([i.repr() for i in self], stream=dumpf)


class FileItem:
    """Representation of a search result. Also retains if the result was visited
    or is marked. Can contain a reference to a `Data` parent in order to make
    the collection autosave in case of a modification.

    """
    __slots__ = (
        "relpath",
        "completepath",
        "line",
        "data",
        "marked",
        "visited",
        "parent",
    )

    def __init__(self, **kwargs):
        if "parent" not in kwargs:
            kwargs["parent"] = None
        for k, v in kwargs.items():
            if k in self.__slots__:
                setattr(self, k, v)
            else:
                raise AttributeError(k)

    def __setattr__(self, k, v):
        # pylint: disable=no-member
        super().__setattr__(k, v)
        if k != "parent" and getattr(self, "parent", None):
            self.parent.save()

    def repr(self):
        # pylint: disable=no-member
        return {
            "relpath": self.relpath,
            "completepath": self.completepath,
            "line": self.line,
            "data": self.data,
            "marked": self.marked,
            "visited": self.visited,
        }


def process_input(filepath, force_reload=False):
    """Load raw or processed data from a file.

    If `force_reload` is true, it will load the file as if it is an
    already-processed data dump file, without checking its extension.

    """
    filepath = filepath or ""
    _, ext = os.path.splitext(filepath)
    if ext in settings["data_dump_extensions"] or force_reload:
        data = _load_dump(filepath)
    else:
        data = _load_raw(open(filepath) if filepath else sys.stdin)
    data.sort(key=lambda fi: (fi.relpath, fi.line))
    return data


def _load_dump(filepath):
    raw_data = yaml.safe_load(open(filepath))
    data = Data()
    for fi in raw_data:
        data.append(FileItem(**fi))
    data.save_path = filepath
    return data


def _load_raw(source):
    input_buffer = [l.strip() for l in source if l.strip()]
    data = Data()
    for line in input_buffer:
        try:
            file_path, lineno, *other = line.split(":")
            lineno = int(lineno)
        except ValueError as ex:
            # This can be given by both split() and int()
            LOG.warning("Error at line\n{}\n{}", line, ex)
        else:
            other = ':'.join(other)
            complete_path = os.path.abspath(file_path)
            new_item = FileItem(
                relpath=file_path, completepath=complete_path,
                line=lineno, data=other, marked=False, visited=False)
            data.append(new_item)
    return data
