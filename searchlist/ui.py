from collections import OrderedDict
import os

import urwid

from searchlist import LOG
from searchlist.config import settings


def main(data):
    Ui(data).run()

class Ui:
    palette = [
        ('body', 'light green', 'default'),
        ('list_mark_empty', 'dark gray', 'default', 'white', 'dark gray', 'default'),
        ('list_path', 'light magenta', 'default', 'white', '#a8f', 'default'),
        ('list_line', 'light cyan', 'default', 'white', '#08f', 'default'),
        ('list_other', 'white', 'default'),
        ('list_mark_full', 'light green', 'default', 'white', '#0d6', 'default'),
        ('list_path_focus', 'yellow', 'default', 'white', '#fa0', 'default'),
        ('list_line_focus', 'light cyan', 'default', 'white', '#6af', 'default'),
        ('list_other_focus', 'white', 'default'),
        ('foot', 'light gray', 'black'),
        ('key', 'light cyan', 'black'),
        ('title', 'white', 'black',),
    ]

    footer_text = [
        " ", ('title', "Searchlist"), "    ",
        ('key', "M/TAB"), " marks;  ",
        ('key', "ENTER/SPC"), " visits link;  ",
        ('key', "Q"), " exits;  ",
    ]

    footer_position_text = "{pos}/{total} results {percent:.1%}"

    def __init__(self, data):
        # pylint: disable=redefined-variable-type
        self.widgets_to_data = self._build_widgets(data)
        self.listbox = urwid.ListBox(urwid.SimpleListWalker(list(self.widgets_to_data)))
        urwid.connect_signal(self.listbox.body, "modified", self._position_changed)

        main_widget = urwid.Padding(urwid.AttrWrap(self.listbox, 'body'),
                                    left=1, right=1)

        help_label = urwid.AttrMap(urwid.Text(self.footer_text), 'foot')
        save_to_edit = urwid.Text("{}  ".format(data.save_path or "EVANESCENT"), align=urwid.RIGHT)
        save_to_edit = urwid.AttrMap(save_to_edit, "foot")
        self.footer_position = urwid.Text(self._progress(1))
        footer_position = urwid.AttrMap(self.footer_position, "foot")
        footer = urwid.AttrMap(
            urwid.Columns([help_label, ("pack", footer_position), save_to_edit]), "foot")

        view = urwid.Frame(main_widget, footer=footer)
        self.loopty_loop = urwid.MainLoop(view, unhandled_input=self.exit_on_q)

        # If you do not have a modern terminal... shame.
        self.loopty_loop.screen.set_terminal_properties(colors=256, has_underline=True)
        self.loopty_loop.screen.register_palette(self.palette)

    def run(self):
        self.loopty_loop.run()

    def exit_on_q(self, input_key):
        if input_key in ('q', 'Q'):
            raise urwid.ExitMainLoop()
        if input_key in ('m', 'M', 'tab'):
            button = self.listbox.focus
            fitem = self.widgets_to_data[button]
            fitem.marked = not fitem.marked
            button.set_label(_label_for(fitem))

    def _position_changed(self, *_):
        # pylint: disable=no-member
        position = self.listbox.focus_position + 1  # Index begins with 0
        self.footer_position.set_text(self._progress(position))

    @classmethod
    def _build_widgets(cls, data):
        widgets_to_data = OrderedDict()
        for fitem in data:
            label = _label_for(fitem)
            widget = urwid.AttrWrap(urwid.Button(label, cls.on_file_item_click, fitem), "body")
            widgets_to_data[widget] = fitem
        return widgets_to_data

    @classmethod
    def on_file_item_click(cls, widg, fitem):
        fitem.visited = True
        command = settings["execute"]
        LOG.debug("File executed: {}", fitem.completepath)
        parameters = {
            "line": fitem.line,
            "path": fitem.completepath
        }
        widg.set_label(_label_for(fitem))
        os.system(command.format(**parameters))

    def _progress(self, index):
        total = len(self.widgets_to_data)
        return self.footer_position_text.format(
            pos=index, total=total, percent=index/total if total else 0)


def _label_for(fitem):
    if fitem.marked:
        mark = ("list_mark_full", "###")
    else:
        mark = ("list_mark_empty", "---")
    label = [
        mark, " ",
        ("list_path" + ("_focus" if fitem.visited else ""), fitem.relpath),
        ("list_line" + ("_focus" if fitem.visited else ""), ":{}: ".format(fitem.line)),
        ("list_other" + ("_focus" if fitem.visited else ""), fitem.data)
    ]
    return label
