import sys
import logging
from os import path
import os

import yaml

from searchlist import LOG


def configure_logger(logger=None, setts=None):
    # pylint: disable=redefined-variable-type
    lvl = logging.DEBUG
    logger = logger or LOG
    if setts:
        handler = logging.FileHandler(setts["logfile"])
    else:
        handler = logging.StreamHandler(stream=sys.stderr)
    handler.setFormatter(logging.Formatter(
        fmt="{asctime}|{levelname}|{filename}|{lineno}: {message}", style="{"))
    handler.setLevel(lvl)

    logger.handlers.clear()
    logger.addHandler(handler)
    logger.setLevel(lvl)
    return logger


def absolute_path(fpath):
    return path.abspath(path.expanduser(fpath))


class _Settings(dict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.reset_to_default()

    def reset_to_default(self):
        self["execute"] = "emacsclient -n +{line} {path}"
        self["datadir"] = absolute_path("~/.searchlist")
        self["logfile"] = self["datadir"] + "/log"
        self["data_dump_extensions"] = (".yml", ".yaml")

    def check_data_dir(self):
        if not os.path.exists(self["datadir"]):
            LOG.info("Data directory {} not found - creating...", self["datadir"])
            os.mkdir(self["datadir"])

    def update_from(self, settings_file):
        LOG.debug("Loading settings data from {}".format(settings_file))
        if os.path.exists(settings_file):
            new_settings = yaml.safe_load(open(settings_file))
            update_config = new_settings.keys() != self.keys()
            self.update(new_settings)
            self.check_data_dir()
            if update_config:
                self.dump_settings(settings_file)
        else:
            self.check_data_dir()
            self.dump_settings(settings_file)

    def dump_settings(self, settings_file):
        with open(settings_file, "w") as out_settings:
            yaml.safe_dump(dict(self), stream=out_settings,
                           default_flow_style=False)

# This is a common value so screw naming conventions.
# pylint: disable=invalid-name
settings = _Settings()
