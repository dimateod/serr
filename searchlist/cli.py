import datetime
import os

import click

from searchlist.config import (
    absolute_path,
    configure_logger,
    settings,
)
from searchlist.textworks import process_input
from searchlist.ui import main as uimain


NOW = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")


@click.command()
@click.argument("filepath", default=None, required=False,
                type=click.Path(file_okay=True, dir_okay=False, readable=True))
@click.option("-c", "--config", default="~/.searchlist/searchlist.conf.yaml",
              type=click.Path(file_okay=True, dir_okay=False, readable=True))
@click.option("-s", "--save-to", default="/tmp/searchlist_{}.yaml".format(NOW),
              type=click.Path(file_okay=True, dir_okay=False, exists=False))
def main(filepath, config, save_to):
    """Tool used to receive the output from a search tool like `grep` or `ag` which
    includes file paths and line numbers and then displays a list of clickable
    links to these files. This can be used to open your favourite text editor
    at those specific points or just execute whatever you want to execute.

    """
    log = configure_logger()

    if config:
        config = absolute_path(config)
        settings.update_from(config)
    configure_logger(setts=settings)
    log.info("--- Hello, Dave ---")

    data = process_input(filepath)
    data.save_path = data.save_path or absolute_path(save_to)

    redirect_stdin()
    uimain(data)


def redirect_stdin():
    """Because the normal stdin might be a pipe that is the stream source of our
    data, we need to retake the stdin from the current console in order to
    enable the user interface. Usually `/dev/tty` is a good source.

    """
    stdin = open("/dev/tty")
    os.dup2(stdin.fileno(), 0)
