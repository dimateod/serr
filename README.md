# serr

Tool used to list the output from a search tool like `grep` or `ag`. This output
must include file paths and line numbers, which are then displayed as a list
with locations which can be opened in your favourite text editor. Alternatively,
you can just run whatever command you want with the file path, line numbers and
the content at that line.

Built and used in Linux distributions with Python 3.

[Project icon](https://www.iconfinder.com/icons/3734490/candle_decoration_flame_glowing_halloween_mystery_wax_icon)
by _Chanut is Industries_ licensed under CC BY 3.0

## Install

```
pip3 install "https://gitlab.com/dimateod/serr/-/archive/master/serr-master.zip"
```

## How to use


### ag

```
ag "search query" --nogroup | serr
```

### grep

```
grep "search query" -nR . | serr
```

As a safety measure, the application will ingest the output of `ag` or `grep`
and can create data dumps which can be used as search query _savepoints_. If one
of these results is created and then the app is closed, `serr` can be used
to reopen the file that contains the data dump of that search.

E.g. We search for an albatross in `/var`:

```
ag albatross --nogroup | serr -s save.yml
```

And we modify the search result list by opening some locations or tagging them,
but then (SURPRISE!) we close the application because we were distracted by the
Spanish Inquisition. We could then recreate the search, but then all of our
progress throughout the files is lost! Fret not though! We can just reload the
data dump:

```
serr save.yml
```

And all our progress is reloaded.

## Config

There are a few things that can be configured from a disk file located at
`~/.searchlist/searchlist.conf.yaml` or wherever the `--config` options points
to. The config format is YAML and its properties are:
- `execute`: What to execute in a child process when a line is selected. This is
  a string which is used as a template to insert the path and the line where the
  result was found. The command variable names are:
    - `path` - The path to the file which contains the search result;
    - `line` - the line number containing the search result; 
- `logfile`: Where to store the application log.
- `data_dump_extensions`: What files to consider being application data dumps.
  These files are considered to be data dumps that were already ingested by the
  program and are not in the format `path:line: text`.


### execute

The default command used is `emacsclient -n +{line} {path}` which opens the
emacs editor to the line where the search string was found.
